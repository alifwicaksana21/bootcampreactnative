import React from 'react';
import { StyleSheet, View, Text, TextInput, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const AboutScreen = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Tentang Saya</Text>
            <Image source={require('./images/profile.png')} style={styles.logo} />
            <View style={{alignItems:'center'}}>
                <Text style={{ fontSize: 36, color: '#27527d', fontWeight: 'bold' }}>Mukhlis Hanafi</Text>
                <Text style={{ color: '#3ec6ff', fontSize: 24 }}>React Native Developer</Text>
            </View>
            <View style={styles.box}>
                <Text style={{ fontSize: 24, color: '#27527d' }}>Portofolio</Text>
                <View style={{
                    alignSelf: 'center',
                    backgroundColor: '#27527d',
                    height: 1,
                    width: 330,
                    marginBottom: 8
                }} />
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                    <View style={{ alignItems: 'center' }}>
                        <Icon name='gitlab' size={50} color='#3ec6ff' />
                        <Text style={styles.profil}>@mukhlish</Text>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Icon name='github' size={50} color='#3ec6ff' />
                        <Text style={styles.profil}>@mukhlis-h</Text>
                    </View>
                </View>
            </View>
            <View style={styles.box}>
                <Text style={{ fontSize: 24, color: '#27527d' }}>Hubungi Saya</Text>
                <View style={{
                    alignSelf: 'center',
                    backgroundColor: '#27527d',
                    height: 1, width: 330,
                    marginBottom: 8
                }} />
                <View style={{ alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', width: 200 }}>
                        <Icon name='facebook-square' size={50} color='#3ec6ff' style={{ marginRight: 32 }} />
                        <Text style={styles.profil}>mukhlis.hanafi</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', width: 200 }}>
                        <Icon name='instagram' size={50} color='#3ec6ff' style={{ marginRight: 32 }} />
                        <Text style={styles.profil}>@mukhlis_hanafi</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', width: 200 }}>
                        <Icon name='twitter' size={50} color='#3ec6ff' style={{ marginRight: 32 }} />
                        <Text style={styles.profil}>@mukhlish</Text>
                    </View>
                </View>
            </View>
            <View style={{ alignItems: 'center' }}>

            </View>
        </View>
    );
}

export default AboutScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 12,
        paddingTop: 32,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    title: {
        fontSize: 48,
        color: '#27527d',
        fontWeight: 'bold'
    },
    logo: {
        width: 150,
        height: 150,
        borderRadius: 150,
        backgroundColor: '#efefef'
    },
    box: {
        backgroundColor: '#efefef',
        borderRadius: 5,
        padding: 12,
        width: 350
    },
    profil: {
        fontSize: 16,
        color: '#27527d',
        fontWeight: 'bold'
    }
})