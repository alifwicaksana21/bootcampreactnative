import React from 'react';
import { StyleSheet, View, Text, TextInput, Image, TouchableOpacity } from 'react-native';

const LoginScreen = () => {
    return (
        <View style={styles.container}>
            <Image source={require('./images/logo.png')} style={styles.logo} />
            <Text style={styles.title}>Login</Text>
            <View>
                <View style={{ marginBottom: 12 }}>
                    <Text style={styles.label}>Username / Email</Text>
                    <TextInput style={styles.input} />
                </View>
                <View>
                    <Text style={styles.label}>Password</Text>
                    <TextInput style={styles.input} />
                </View>
            </View>
            <View style={{ alignItems: 'center' }}>
                <TouchableOpacity>
                    <View style={styles.buttonLog}>
                        <Text style={styles.buttonText}>Masuk</Text>
                    </View>
                </TouchableOpacity>
                <Text style={{ color: '#3ec6ff', fontSize: 24, marginVertical: 8 }}>atau</Text>
                <TouchableOpacity>
                    <View style={styles.buttonReg}>
                        <Text style={styles.buttonText}>Daftar ?</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 24,
        paddingTop: 32,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    title: {
        fontSize: 48,
        color: '#27527d',
        fontWeight: '600'
    },
    logo: {
        width: 375,
        height: 116,
    },
    label: {
        color: '#27527d',
        fontSize: 18,
        fontWeight: 'bold'
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 4,
        width: 300,
        height: 40,
        borderColor: '#003065',
        borderWidth: 2
    },
    buttonLog: {
        width: 150,
        height: 40,
        backgroundColor: '#3ec6ff',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 18
    },
    buttonReg: {
        width: 150,
        height: 40,
        backgroundColor: '#003366',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 18
    },
    buttonText: {
        color: 'white',
        fontSize: 24
    }
})