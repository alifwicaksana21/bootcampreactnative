import React from 'react';
import { StyleSheet, View, Text, TextInput, Image, TouchableOpacity } from 'react-native';

const LoginScreen = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>LoginScreen</Text>
            <Image source={require('./images/4F0GaDS.png')} style={styles.logo} />
            <View>
                <TextInput placeholder='Username / Email' placeholderTextColor='#566246' style={styles.input} />
                <TextInput placeholder='Password' placeholderTextColor='#566246' style={styles.input} />
                <TextInput placeholder='Confirm Password' placeholderTextColor='#566246' style={styles.input} />
            </View>
            <TouchableOpacity>
                <View style={styles.button}>
                    <Text style={styles.buttonText}>Login / Register</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#A4C2A5',
        padding: 24,
        paddingTop: 32,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    title: {
        fontSize: 48,
        color: '#566246',
        fontWeight: '600'
    },
    logo: {
        width: 200,
        height: 200,
        borderRadius: 200
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 4,
        width: 300,
        height: 40,
        borderColor: '#566246',
        borderRadius: 10
    },
    button:{
        width:150,
        height:40,
        backgroundColor:'#566246',
        alignItems:'center',
        justifyContent:'center',
        borderColor:'#A4C2A5',
        borderWidth:2,
        borderRadius:10,
        elevation:5
    }
})