import * as React from "react";
import { View, Text } from "react-native";
import LoginScreen from "./Tugas/Tugas13/LoginScreen";
import RegisterScreen from "./Tugas/Tugas13/RegisterScreen";
import AboutScreen from "./Tugas/Tugas13/AboutScreen";

export default function App() {
  return (
    // <LoginScreen /> //Menampilkan Page Login
    // <RegisterScreen /> //Menampilkan Page Register
    <AboutScreen /> //Menampilkan Page About
  );
}
