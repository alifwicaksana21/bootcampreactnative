var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise
let sisaWaktu = 10000
let i = 0

readBooksPromise(sisaWaktu, books[i])
    .then(function (waktu) {
        i++;
        readBooksPromise(waktu, books[i])
            .then(function (waktu) {
                if (i > books.length) i = 0;
                i++;
                readBooksPromise(waktu, books[i])
                    .then(function (waktu) {
                        if (i > books.length) i = 0;
                        readBooksPromise(waktu, books[i])
                            .then(function (waktu) {

                            }).catch(function (error) {
                                // console.log(error)
                            })
                    }).catch(function (error) {
                        // console.log(error)
                    })
            }).catch(function (error) {
                // console.log(error)
            })
    }).catch(function (error) {
        console.log(error)
    })
