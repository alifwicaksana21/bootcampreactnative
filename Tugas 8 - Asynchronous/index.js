// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Tulis code untuk memanggil function readBooks di sini
let waktu = 10000
let i = 0

readBooks(waktu, books[i], read = (time) => {
    i++
    if (i > 2) i = 0;

    if (waktu != time) {
        waktu = time
        readBooks(waktu, books[i], read)
    }
})