import * as React from "react";
import { View, Image, TouchableOpacity, Text, StyleSheet, StatusBar, FlatList } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'
import VideoItem from './components/videoitem'
import data from './data.json'

export default function App() {
    return (
        <View
            style={styles.container}
        >
            <StatusBar backgroundColor='black' />
            <View style={styles.navBar}>
                <Image source={require('./images/logo.png')}
                    style={{ width: 98, height: 22 }} />
                <View style={styles.rightNav}>
                    <TouchableOpacity>
                        <Icon style={styles.navItem} name='search' size={30} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon style={styles.navItem} name='account-circle' size={30} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.body}>
                <FlatList
                    data={data.items}
                    renderItem={(video) => <VideoItem video={video.item} />}
                    keyExtractor={(item) => item.id}
                    ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#e5e5e5'}}/>}
                />
            </View>
            <View style={styles.tabBar}>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name='home' size={25} />
                    <Text style={styles.tabTitle}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name='whatshot' size={25} />
                    <Text style={styles.tabTitle}>Trending</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name='subscriptions' size={25} />
                    <Text style={styles.tabTitle}>Subscription</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name='folder' size={25} />
                    <Text style={styles.tabTitle}>Library</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rightNav: {
        flexDirection: 'row'
    },
    navItem: {
        marginLeft: 25
    },
    body: {
        flex: 1
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderTopColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle: {
        color: '#3c3c3c',
        fontSize: 11,
        paddingTop: 4
    }
})