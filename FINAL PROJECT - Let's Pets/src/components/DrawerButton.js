import React from 'react';
import { TouchableHighlight, View, Text } from 'react-native';
import Constants from 'expo-constants';

const deviceId = Constants.deviceId;

const onLogout = () => {
    fetch(`https://projekbareng.com/logout.php?uuid=${deviceId}`);

}

const DrawerButton = (props) => {
    console.log(this);
    return (
        <TouchableHighlight
            underlayColor='gray'
            onPress={() => alert('Pressed')}
            style={{
                backgroundColor: 'white',
                borderRadius: 5,
                marginHorizontal: 10,
                marginVertical: 5,
                paddingHorizontal: 8,
                height: 40,
                justifyContent: 'center'
            }}
        >

            <Text style={{ color: '#FF0000' }}>Logout</Text>
        </TouchableHighlight>
    )
}

export default DrawerButton;