import React from 'react';
import { View, Image } from 'react-native';

const DrawerProfile = () => {
    return (
        <View style={{ alignItems: 'center', marginBottom: 30 }}>
            <Image
                source={require('../images/profile_pic.png')}
                style={{ width: 100, height: 100, borderRadius: 50 }}
            />
        </View>
    )
}

export default DrawerProfile;