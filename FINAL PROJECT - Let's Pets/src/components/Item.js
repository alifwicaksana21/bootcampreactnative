import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';

const currencyFormat = (num) => {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

const Item = ({ name, img_path, price, onPress }) => {
    price = currencyFormat(Number(price));
    return (
        <View style={styles.container}>
            <TouchableOpacity
                onPress={() => { onPress() }}
            >
                <Image source={{ uri: img_path }} style={{ width: 150, height: 150 }} />
                <View>
                    <Text style={styles.name}>{name}</Text>
                    <Text style={styles.price}>{price}</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default Item;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 8,
        paddingHorizontal: 16,
        borderRadius: 8,
        margin: 8,
        elevation: 5
    },
    name: {
        fontSize: 18,
        maxWidth: 150,
    },
    price: {
        fontSize: 20,
        color: 'gray'
    }
})