import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import Constants from 'expo-constants';

const deviceId = Constants.deviceId;

const SplashScreen = ({ navigation }) => {
    setTimeout(() => {
        fetch(`https://projekbareng.com/letspets/checklogin.php?uuid=${deviceId}`)
            .then((res) => res.json())
            .then((json) => {
                if (json.status == 'OK') {
                    navigation.navigate('Drawer')
                } else {
                    navigation.navigate('Welcome');
                }
            });
    }, 500)
    return (
        <View style={styles.container}>
            <Image
                source={require('../images/logo.png')}
                style={{ width: 300, height: 300 }}
            />
        </View>
    );
}

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E29578',
        alignItems: 'center',
        justifyContent: 'center'
    },
})