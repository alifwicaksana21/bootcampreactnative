import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import DrawerProfile from "../components/DrawerProfile";
import Constants from 'expo-constants';

const deviceId = Constants.deviceId;

const AboutScreen = ({ navigation }) => {
    
    const onLogout = () => {
        fetch(`https://projekbareng.com/letspets/logout.php?uuid=${deviceId}`);
        navigation.navigate('Welcome');
    }
    console.log(navigation)
    return (
        <View style={styles.container}>
            <Text>AboutScreen</Text>
            <DrawerProfile />
            <Image source={require('../images/bg_ikan.jpg')}
                style={{ width: 100, height: 100 }} />
            <TouchableOpacity onPress={() => onLogout()}>
                <View>
                    <Text>Logout</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default AboutScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})