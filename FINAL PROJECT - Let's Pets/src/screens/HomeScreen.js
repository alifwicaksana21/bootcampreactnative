import React, { useState, useEffect } from "react";
import { StyleSheet, View, FlatList, Dimensions, StatusBar, ScrollView, TextInput, TouchableOpacity, LogBox } from 'react-native';
import Constants from 'expo-constants';
import Icon from "react-native-vector-icons/Ionicons";
import { SliderBox } from "react-native-image-slider-box";
import Item from "../components/Item";

LogBox.ignoreAllLogs();

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const deviceId = Constants.deviceId;
const images = [
    require('../images/bg_kucing.jpg'),
    require('../images/bg_anjing.jpeg'),
    require('../images/bg_sugarglider.jpg'),
    require('../images/bg_ikan.jpg'),
    require('../images/bg_marmut.jpg'),
    require('../images/bg_owl.jpg')
];

const HomeScreen = ({ navigation }) => {
    const [items, setItems] = useState([]);

    useEffect(() => {
        fetch("http://projekbareng.com/letspets/items.php")
            .then((res) => res.json())
            .then((json) => {
                setItems(json);
                // console.log(json)
            });
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.searchBar}>
                <TouchableOpacity onPress={() => { navigation.toggleDrawer() }}>
                    <Icon name="reorder-four" size={30} style={{ marginHorizontal: 10, color: '#83C5BE' }} />
                </TouchableOpacity>
                <TextInput style={styles.searchText} placeholder="Search" />
                <TouchableOpacity>
                    <Icon name="heart" size={25} style={{ marginHorizontal: 8, color: '#83C5BE' }} />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon name="notifications" size={25} style={{ marginHorizontal: 8, color: '#83C5BE' }} />
                </TouchableOpacity>
            </View>
            <ScrollView style={{ flex: 1 }}>
                <View>
                    <SliderBox
                        images={images}
                        sliderBoxHeight={250}
                        dotColor="#E29578"
                        inactiveDotColor="#FFDDD2"
                        paginationBoxVerticalPadding={20}
                        circleLoop
                        autoplay
                    />
                    <View style={styles.body}>
                        <FlatList
                            nestedScrollEnabled={true}
                            data={items}
                            keyExtractor={(item, idx) => idx}
                            renderItem={({ item }) => (
                                <Item
                                    name={item.name}
                                    price={item.price}
                                    img_path={item.img_path}
                                    onPress={() => { navigation.navigate('Detail',{id:item.id}) }}
                                />
                            )}
                            numColumns={2}
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    searchBar: {
        backgroundColor: '#006D77',
        height: 60,
        flexDirection: 'row',
        width: windowWidth,
        padding: 4,
        paddingHorizontal: 10,
        alignItems: 'center',
        marginTop: StatusBar.currentHeight,
    },
    searchText: {
        backgroundColor: 'white',
        marginRight: 8,
        paddingLeft: 20,
        borderRadius: 40,
        height: 40,
        flex: 1,
        width: windowWidth / 1.5
    },
    body: {
        alignItems: 'center',
        justifyContent: 'center',
        height: windowHeight,
    }
});