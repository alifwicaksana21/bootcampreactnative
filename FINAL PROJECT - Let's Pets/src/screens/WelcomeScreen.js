import React, { useState, useEffect } from "react";
import {
    Dimensions,
    Modal,
    StyleSheet,
    Text,
    TextInput,
    TouchableHighlight,
    View,
    Image,
    StatusBar
} from "react-native";
import { HelperText } from 'react-native-paper';
import Constants from 'expo-constants';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const deviceId = Constants.deviceId;

const WelcomeScreen = ({ navigation }) => {
    const [modalWelcome, setModalWelcome] = useState(true);
    const [modalLogin, setModalLogin] = useState(false);
    const [modalRegister, setModalRegister] = useState(false);
    const [textFname, setTextFname] = useState('');
    const [textLname, setTextLname] = useState('');
    const [textEmail, setTextEmail] = useState('');
    const [textPassword, setTextPassword] = useState('');
    const [textPassword2, setTextPassword2] = useState('');
    const [errFname, setErrFname] = useState(false);
    const [errLname, setErrLname] = useState(false);
    const [errEmail, setErrEmail] = useState(false);
    const [errPassword, setErrPassword] = useState(false);
    const [msgerrFname, setMsgErrFname] = useState('');
    const [msgerrLname, setMsgErrLname] = useState('');
    const [msgerrEmail, setMsgErrEmail] = useState('');
    const [msgerrPassword, setMsgErrPassword] = useState('');

    useEffect(() => {
        setModalWelcome(true);
        setModalLogin(false);
        setModalRegister(false);
    }, []);

    const onLogin = () => {
        let formData = new FormData();
        formData.append('email', textEmail);
        formData.append('password', textPassword);
        formData.append('uuid', deviceId);

        fetch("https://projekbareng.com/letspets/login.php", {
            method: 'POST',
            body: formData
        })
            .then((res) => res.json())
            .then((json) => {
                console.log(json)
                if (json.status == 'OK') {
                    setModalLogin(false);
                    navigation.push('Drawer');
                } else {
                    switch (json.err) {
                        case 'email': setErrEmail(true); setMsgErrEmail(json.msg); break;
                        case 'pass': setErrPassword(true); setMsgErrPassword(json.msg); break;
                    }
                }
            })
    }

    const onRegister = () => {
        let formData = new FormData();
        formData.append('fname', textFname);
        formData.append('lname', textLname);
        formData.append('email', textEmail);
        formData.append('password1', textPassword);
        formData.append('password2', textPassword2);

        console.log('start')
        fetch("https://projekbareng.com/letspets/register.php", {
            method: 'POST',
            body: formData
        })
            .then((res) => res.json())
            .then((json) => {
                if (json.status == 'OK') {
                    setModalRegister(false);
                    setModalLogin(true);
                } else {
                    // console.log(json)
                    for (let i = 0; i < json.length; i++) {
                        switch (json[i].err) {
                            case 'fname': setErrFname(true); setMsgErrFname(json[i].msg); break;
                            case 'lname': setErrLname(true); setMsgErrLname(json[i].msg); break;
                            case 'email': setErrEmail(true); setMsgErrEmail(json[i].msg); break;
                            case 'pass': setErrPassword(true); setMsgErrPassword(json[i].msg); break;
                        }
                    }
                }
            })
    }

    return (
        <View style={styles.container}>
            <Image
                source={require('../images/bg.jpeg')}
                style={{ width: windowWidth, height: windowHeight }}
            />
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalWelcome}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalWelcome}>
                        <Text style={styles.title}>Welcome</Text>
                        <TouchableHighlight
                            style={styles.regButton}
                            onPress={() => {
                                setModalWelcome(false);
                                setModalRegister(true);
                                setErrEmail(false);
                                setErrPassword(false);
                            }}
                        >
                            <Text style={styles.textStyle}>Register</Text>
                        </TouchableHighlight>
                        <TouchableHighlight
                            style={styles.logButton}
                            onPress={() => {
                                setModalWelcome(false);
                                setModalLogin(true);
                            }}
                        >
                            <Text style={styles.textStyle}>Login</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalLogin}
                onRequestClose={() => {
                    setModalLogin(false);
                    setModalWelcome(true);
                    setTextEmail('');
                    setTextPassword('');
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalLogin}>
                        <Text style={styles.title}>Sign In</Text>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={textEmail => {
                                setTextEmail(textEmail.replace(' ', ''));
                                setErrEmail(false);
                            }}
                            value={textEmail}
                            placeholder="Email"
                        />
                        <HelperText type='error' visible={errEmail}>{msgerrEmail}</HelperText>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={(textPassword) => {
                                setTextPassword(textPassword);
                                setErrPassword(false);
                            }}
                            value={textPassword}
                            secureTextEntry={true}
                            placeholder="Password"
                        />
                        <HelperText type='error' visible={errPassword}>{msgerrPassword}</HelperText>
                        <View></View>
                        <TouchableHighlight
                            style={styles.logButton}
                            onPress={() => {
                                onLogin();
                            }}
                        >
                            <Text style={styles.textStyle}>Sign In</Text>
                        </TouchableHighlight>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: windowWidth / 1.5 }}>
                            <Text style={{ color: '#83C5BE' }}>Forgot Password?</Text>
                            <Text>Sign Up</Text>
                        </View>
                    </View>
                </View>
            </Modal>

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalRegister}
                onRequestClose={() => {
                    setModalRegister(false);
                    setModalWelcome(true);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalRegister}>
                        <Text style={styles.title}>Sign Up</Text>
                        <View>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => {
                                    setTextFname(text);
                                    setErrFname(false);
                                }}
                                value={textFname}
                                placeholder="First Name"
                            />
                            <HelperText type='error' visible={errFname}>{msgerrFname}</HelperText>
                        </View>
                        <View>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => {
                                    setTextLname(text);
                                    setErrLname(false);
                                }}
                                value={textLname}
                                placeholder="Last Name"
                            />
                            <HelperText type='error' visible={errLname}>{msgerrLname}</HelperText>
                        </View>
                        <View>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={textEmail => {
                                    setTextEmail(textEmail);
                                    setErrEmail(false);
                                }}
                                value={textEmail}
                                placeholder="Email"
                            />
                            <HelperText type='error' visible={errEmail}>{msgerrEmail}</HelperText>
                        </View>
                        <View>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={textPassword => {
                                    setTextPassword(textPassword);
                                    setErrPassword(false);
                                }}
                                value={textPassword}
                                secureTextEntry={true}
                                placeholder="Password"
                            />
                            <HelperText type='error' visible={errPassword}>{msgerrPassword}</HelperText>
                        </View>
                        <View>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => {
                                    setTextPassword2(text);
                                    setErrPassword(false);
                                }}
                                value={textPassword2}
                                secureTextEntry={true}
                                placeholder="Confirm Password"
                            />
                            <HelperText type='error' visible={errPassword}>{msgerrPassword}</HelperText>
                        </View>
                        <View></View>
                        <TouchableHighlight
                            style={styles.regButton}
                            onPress={() => {
                                onRegister();
                            }}
                        >
                            <Text style={styles.textStyle}>Sign Up</Text>
                        </TouchableHighlight>
                        <Text style={{ color: '#83C5BE' }}>Already have an account?</Text>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        marginTop:StatusBar.currentHeight
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center"
    },
    modalWelcome: {
        backgroundColor: "white",
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        padding: 20,
        paddingBottom: 50,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        height: 250,
        width: windowWidth,
        justifyContent: 'space-around'
    },
    modalLogin: {
        backgroundColor: "white",
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        padding: 10,
        paddingBottom: 50,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        height: 400,
        width: windowWidth,
        justifyContent: 'space-evenly'
    },
    modalRegister: {
        backgroundColor: "white",
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        padding: 10,
        paddingBottom: 50,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        height: 600,
        width: windowWidth,
        justifyContent: 'space-evenly'
    },
    title: {
        fontSize: 38,
        textAlign: 'left',
        width: windowWidth / 1.5
    },
    textInput: {
        width: windowWidth / 1.5,
        paddingHorizontal: 10,
        fontSize: 18,
        borderBottomWidth: 1,
        borderBottomColor: '#83C5BE'
    },
    regButton: {
        backgroundColor: "#006D77",
        borderRadius: 10,
        padding: 10,
        height: windowHeight / 17,
        width: windowWidth / 1.5,
        justifyContent: 'center'
    },
    logButton: {
        backgroundColor: "#006D77",
        borderRadius: 10,
        padding: 10,
        height: windowHeight / 17,
        width: windowWidth / 1.5,
        justifyContent: 'center'
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 18
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});

export default WelcomeScreen;