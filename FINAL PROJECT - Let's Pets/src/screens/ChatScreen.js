import React from 'react';
import { StyleSheet, View, Image } from 'react-native';

const ChatScreen = () => {
    return (
        <View style={styles.container}>
            <Image
                source={require('../images/logo.png')}
                style={{ width: 300, height: 300 }}
            />
        </View>
    );
}

export default ChatScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E29578',
        alignItems: 'center',
        justifyContent: 'center'
    },
})