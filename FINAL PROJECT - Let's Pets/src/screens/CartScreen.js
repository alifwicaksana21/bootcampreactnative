import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

const BasketScreen = () => {
    return (
        <View style={styles.container}>
            <Text>BasketScreen</Text>
        </View>
    )
}

export default BasketScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})