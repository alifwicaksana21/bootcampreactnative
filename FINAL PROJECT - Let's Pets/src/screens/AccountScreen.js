import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Constants from 'expo-constants';

const deviceId = Constants.deviceId;

const AccountScreen = ({ navigation }) => {
    const saldo = 20000;

    const onLogout = () => {
        fetch(`https://projekbareng.com/letspets/logout.php?uuid=${deviceId}`);
        navigation.navigate('Welcome');
    }
    console.log(navigation)
    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <Text style={styles.titleText}>My Account</Text>
                <View style={{ flexDirection: 'row', width: 100, justifyContent: 'space-between' }}>
                    <Icon name='notifications' size={30} />
                    <Icon name='chatbox-ellipses-outline' size={30} />
                    <Icon name='settings' size={30} />
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <Text style={{ fontSize: 24 }}>Alif Wicaksana Ramadhan</Text>
            </View>
            <View style={styles.div1}>
                <Image
                    source={require('../images/profile_pic.png')}
                    style={{ width: 70, height: 70, borderRadius: 30 }}
                />
                <Text style={{ fontSize: 24 }}>Saldo :</Text>
                <Text style={{ fontSize: 24 }}>{saldo}</Text>
            </View>
            <View style={styles.div2}>

            </View>

            <View style={{ padding: 12 }}>
            </View>

            <TouchableOpacity onPress={() => onLogout()}>
                <View style={styles.logout}>
                    <Text>Logout</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default AccountScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight
    },
    title: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 12,
        backgroundColor: 'white',
        elevation: 5
    },
    titleText: {
        fontSize: 42,
        fontWeight: 'bold'
    },
    div1: {
        paddingVertical: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        elevation: 5
    },
    logout: {
        backgroundColor: 'white',
        justifyContent: 'space-around',
        height: 40,
        elevation: 5,
        padding: 12
    }
})