import React from 'react';
import { useEffect, useState } from 'react';
import { View, ScrollView, StyleSheet, Text, TouchableOpacity, Image, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const currencyFormat = (num) => {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

const DetailScreen = ({ route }) => {
    const [data, setData] = useState({ name: '', price: '', username: '', description: '', img_path: '' });
    const [isLoading, setIsLoading] = useState(true);
    const item_id = route.params.id;

    useEffect(() => {
        fetch(`https://projekbareng.com/letspets/itemdetails.php?item_id=${item_id}`)
            .then((res) => res.json())
            .then((json) => {
                setData(json);
                // console.log(json)
                setIsLoading(false);
            })
    }, [])
    if (isLoading) {
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text>Loading</Text>
            </View>
        )
    }
    else {
        return (
            <View style={styles.container}>
                <View style={{
                    position: 'absolute',
                    elevation: 10,
                    flexDirection: 'row',
                    padding: 8,
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                    <TouchableOpacity onPress={() => console.log('back')}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: 12, height: 45, width: 45, backgroundColor: 'rgba(125,125,125,0.25)', borderRadius: 20 }}>
                            <Icon name='arrow-back' size={40} color='gray' />
                        </View>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }}></View>
                    <TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: 8, height: 45, width: 45, backgroundColor: 'rgba(125,125,125,0.25)', borderRadius: 20 }}>
                            <Icon name='heart' size={40} color='gray' />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: 8, height: 45, width: 45, backgroundColor: 'rgba(125,125,125,0.25)', borderRadius: 20 }}>
                            <Icon name='cart' size={40} color='gray' />
                        </View>
                    </TouchableOpacity>
                </View>
                <ScrollView style={{ height: 450 }}>
                    <View>
                        <Image source={{ uri: data.img_path }} style={{ width: windowWidth, height: windowWidth }} />
                        <View style={styles.title}>
                            <Text style={styles.titleText}>{data.name}</Text>
                            <Text style={styles.priceText}>{currencyFormat(Number(data.price))}</Text>
                        </View>
                        <View style={styles.detil}>
                            <Text style={styles.descText}>{data.description}</Text>
                        </View>

                    </View>
                </ScrollView>
                <View style={styles.buyTab}>
                    <TouchableOpacity>
                        <View style={{ ...styles.buyButton, width: 40 }}>
                            <Icon name='chatbox-ellipses-outline' size={20} color='#E29578' />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.buyButton}>
                            <Text style={{ fontSize: 18, color: '#E29578' }}>Beli Sekarang</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{ ...styles.buyButton, backgroundColor: '#E29578' }}>
                            <Icon name='cart' size={20} color='white' />
                            <Text style={{ color: '#EDF6F9' }}>+ Keranjang</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default DetailScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30,
        backgroundColor: '#EDF6F9'
    },
    title: {
        backgroundColor: 'white',
        flex: 0.2,
        width: windowWidth,
        padding: 10,
        elevation: 5
    },
    detil: {
        backgroundColor: 'white',
        flex: 0.8,
        width: windowWidth,
        padding: 18,
        elevation: 5,
        marginTop: 8
    },
    buyTab: {
        backgroundColor: 'white',
        width: windowWidth,
        // height:100,
        flex: 0.25,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 10,
        elevation: 10
    },
    descText: {
        fontSize: 18
    },
    buyButton: {
        borderColor: '#E29578',
        borderWidth: 2,
        height: 40,
        width: windowWidth / 2.5,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        flexDirection: 'row'
    },
    titleText: {
        fontSize: 24
    },
    priceText: {
        fontSize: 32,
        color: '#E29578'
    }
})