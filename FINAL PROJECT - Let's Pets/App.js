import 'react-native-gesture-handler';
import * as React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/Ionicons';
import WelcomeScreen from './src/screens/WelcomeScreen';
import SplashScreen from "./src/screens/SplashScreen";
import HomeScreen from "./src/screens/HomeScreen";
import AccountScreen from "./src/screens/AccountScreen";
import CartScreen from "./src/screens/CartScreen";
import ChatScreen from "./src/screens/ChatScreen";
import FeedScreen from "./src/screens/FeedScreen";
import AboutScreen from "./src/screens/AboutScreen";
import DrawerButton from "./src/components/DrawerButton";
import DrawerProfile from "./src/components/DrawerProfile";
import DetailScreen from "./src/screens/DetailScreen";

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const DrawerScreen = () => {
  return (
    <Drawer.Navigator
      drawerContentOptions={{
        contentContainerStyle: { flex: 1, backgroundColor: 'white' },
        activeTintColor: '#006D77',
        inactiveTintColor: '#83C5BE',
        itemStyle: { height: 40 }
      }}
      drawerContent={props => {
        return (
          <DrawerContentScrollView {...props}>
            <DrawerProfile />
            <DrawerItemList {...props} />
            <DrawerButton />
          </DrawerContentScrollView>
        )
      }}>
      <Drawer.Screen name='Tabs' component={TabsScreen} options={{
        drawerLabel: 'Home'
      }} />
      <Drawer.Screen name='About' component={AboutScreen} />
    </Drawer.Navigator>
  )
}

const TabsScreen = () => {
  return (
    <Tabs.Navigator
      tabBarOptions={{
        activeTintColor: '#006D77',
        inactiveTintColor: '#83C5BE',
        style: { height: 70, paddingBottom: 10, paddingTop: 5 }
      }}>
      <Tabs.Screen name='Home' component={HomeScreen} options={{
        tabBarIcon: ({ color }) => (
          <Icon name='home' color={color} size={24} />
        )
      }} />
      <Tabs.Screen name='Feed' component={FeedScreen} options={{
        tabBarIcon: ({ color }) => (
          <Icon name='list' color={color} size={24} />
        )
      }} />
      <Tabs.Screen name='Chat' component={ChatScreen} options={{
        tabBarIcon: ({ color }) => (
          <Icon name='mail' color={color} size={24} />
        )
      }} />
      <Tabs.Screen name='Cart' component={CartScreen} options={{
        tabBarIcon: ({ color }) => (
          <Icon name='ios-cart' color={color} size={24} />
        )
      }} />
      <Tabs.Screen name='Profile' component={AccountScreen} options={{
        tabBarIcon: ({ color }) => (
          <Icon name='person' color={color} size={24} />
        )
      }} />
    </Tabs.Navigator>
  )
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Splash' headerMode='none'>
        <Stack.Screen name='Splash' component={SplashScreen} />
        <Stack.Screen name='Welcome' component={WelcomeScreen} />
        <Stack.Screen name='Drawer' component={DrawerScreen} />
        <Stack.Screen name='Detail' component={DetailScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
