import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import SkillScreen from './SkillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';
import AboutScreen from './AboutScreen';
import LoginScreen from './LoginScreen';

const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Root = createStackNavigator();

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillScreen} />
        <Tabs.Screen name="Project" component={ProjectScreen} />
        <Tabs.Screen name="Add" component={AddScreen} />
    </Tabs.Navigator>
);

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Tabs" component={TabsScreen} />
        <Drawer.Screen name="About" component={AboutScreen} />
    </Drawer.Navigator>
);

const RootScreen = () => (
    <Root.Navigator>
        <Root.Screen name="Login" component={LoginScreen} />
        <Root.Screen name="Drawer" component={DrawerScreen} />
    </Root.Navigator>
)

const Index = () => {
    return (
        <NavigationContainer>
            <RootScreen />
        </NavigationContainer>
    )
}

export default Index;