console.log('//Soal No. 1 (Array to Object)')

function arrayToObject(arr) {
    // Code di sini 
    var now = new Date()
    var thisYear = now.getFullYear()
    var objects = []
    for (var i = 0; i < arr.length; i++) {
        var age = thisYear - arr[i][3]
        if (age < 0 || arr[i][3] == undefined) {
            age = 'Invalid Birth Year'
        }
        var object = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: age
        }
        console.log((i + 1) + '. ' + object.firstName + ' ' + object.lastName + ' : ')
        console.log(object)
        objects.push(object)
    }
    console.log('')
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

console.log('//Soal No.2 (Shopping Time)')

function shoppingTime(memberId, money) {
    // you can only write your code here!
    console.log('')
    var harga = {
        sepatu_stacattu: 1500000,
        baju_zoro: 500000,
        baju_hn: 250000,
        sweater_uniklooh: 175000,
        casing_hp: 50000
    }
    var listPurchased = []
    var output = {}
    if (memberId === undefined || memberId === '') {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    }
    output.memberId = memberId
    output.money = money
    while (money >= harga.casing_hp) {
        if (money >= harga.sepatu_stacattu) {
            if (!listPurchased.includes('Sepatu Stacattu')) {
                listPurchased.push('Sepatu Stacattu')
                money -= harga.sepatu_stacattu
            }
        } else if (money >= harga.baju_zoro) {
            if (!listPurchased.includes('Baju Zoro')) {
                listPurchased.push('Baju Zoro')
                money -= harga.baju_zoro
            }
        } else if (money >= harga.baju_hn) {
            if (!listPurchased.includes('Baju H&N')) {
                listPurchased.push('Baju H&N')
                money -= harga.baju_hn
            }
        } else if (money >= harga.sweater_uniklooh) {
            if (!listPurchased.includes('Sweater Uniklooh')) {
                listPurchased.push('Sweater Uniklooh')
                money -= harga.sweater_uniklooh
            }
        } else if (money >= harga.casing_hp) {
            if (!listPurchased.includes('Casing Handphone')) {
                listPurchased.push('Casing Handphone')
                money -= harga.casing_hp
            } else {
                break;
            }
        }
    }
    output.listPurchased = listPurchased
    output.changeMoney = money
    return output
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log()
console.log('//Soal No. 3 (Naik Angkot)')

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var objects = []
    for(var i=0;i<arrPenumpang.length;i++){
        var bayar = (rute.indexOf(arrPenumpang[i][2])-rute.indexOf(arrPenumpang[i][1]))*2000
        var objek = {
            penumpang:arrPenumpang[i][0],
            naikDari:arrPenumpang[i][1],
            tujuan:arrPenumpang[i][2],
            bayar:bayar
        }
        objects.push(objek)
    }
    return objects
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]