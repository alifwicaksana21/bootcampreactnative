import * as React from "react";
import { View, Text } from "react-native";

import Index from './src/index';

export default function App() {
  return (
    <Index />
  );
}
