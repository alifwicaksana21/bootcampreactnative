console.log('//No.1 Looping While');

console.log('LOOPING PERTAMA')
var i = 0;
while (i < 20) {
    i += 2
    console.log(i + ' - I love coding');
}
while (i >= 2) {
    console.log(i + ' - I will becone a mobile developer');
    i -= 2
}

console.log('//No.2 Looping for');

for (var i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 != 0) {
        console.log(i + ' - I Love Coding')
    } else if (i % 2 == 0) {
        console.log(i + ' - Berkualitas')
    } else if (i % 2 != 0) {
        console.log(i + ' - Santai')
    }
}

console.log('//No.3 Membuat Persegi Panjang')
var lebar = 8
var tinggi = 4
var baris = ''

for (var i = 0; i < tinggi; i++) {
    for (var j = 0; j < lebar; j++) {
        baris = baris + '#'
    }
    console.log(baris)
    baris = ''
}

console.log('//No.4 Membuat Tangga')
var tinggi = 7
var baris = ''

for (var i = 0; i < tinggi; i++) {
    for (var j = 0; j <= i; j++) {
        baris = baris + '#'
    }
    console.log(baris);
    baris = ''
}

console.log('//No.5 Membuat Papan Catur')
var lebar = 8
var baris = ''

for (var i = 0; i < lebar; i++) {
    if (i % 2 == 0) {
        for (var j = 0; j < lebar; j++) {
            if (j % 2 == 0) {
                baris = baris + ' '
            } else {
                baris = baris + '#'
            }
        }
    } else {
        for (var j = 0; j < lebar; j++) {
            if (j % 2 == 0) {
                baris = baris + '#'
            } else {
                baris = baris + ' '
            }
        }
    }
    console.log(baris);
    baris = ''
}