console.log('//1. Animal Class')
class Animal {
    // Code class di sini
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }

    get name() {
        return this._name
    }

    get legs() {
        return this._legs
    }

    get cold_blooded() {
        return this._cold_blooded
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Frog extends Animal {
    constructor(name) {
        super()
        this._name = name
    }

    get name() {
        return this._name
    }

    get legs() {
        return this._legs
    }

    get cold_blooded() {
        return this._cold_blooded
    }

    jump() {
        console.log('hop hop')
    }
}

class Ape extends Animal {
    constructor(name) {
        super()
        this._name = name
        this._legs = 2
    }

    get name() {
        return this._name
    }

    get legs() {
        return this._legs
    }

    get cold_blooded() {
        return this._cold_blooded
    }

    yell() {
        console.log('Auooo')
    }
}

// Code class Ape dan class Frog di sini

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"


console.log('//2. Function to Class')
class Clock {
    // Code di sini
    constructor({ template }) {
        this._template = template
    }

    render(template) {
        this._date = new Date()
        this._hours = this._date.getHours()

        if (this._hours < 10) this._hours = '0' + this._hours

        this._mins = this._date.getMinutes()
        if (this._mins < 10) this._mins = '0' + this._mins

        this._secs = this._date.getSeconds()
        if (this._secs < 10) this._secs = '0' + this._secs

        this._output = template
            .replace('h', this._hours)
            .replace('m', this._mins)
            .replace('s', this._secs)

        console.log(this._output)
    }

    stop() {
        clearInterval(this._timer)
    }

    start() {
        this.render(this._template)
        this._timer = setInterval(() => this.render(this._template),
            1000)
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start(); 