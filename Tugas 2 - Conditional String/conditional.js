console.log('//Soal 1 (if-else)');

var nama = ""
var peran = ""

if (nama === '') {
    console.log('Nama harus diisi!');
} else {
    if (peran === '') {
        console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
    } else if (peran === 'Penyihir') {
        console.log('Selamat datang di Dunia Wewewolf, ' + nama);
        console.log('Halo ' + peran + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!')
    } else if (peran === 'Guard') {
        console.log('Selamat datang di Dunia Wewewolf, ' + nama);
        console.log('Halo ' + peran + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf!')
    } else if (peran === 'Werewolf') {
        console.log('Selamat datang di Dunia Wewewolf, ' + nama);
        console.log('Halo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!')
    }
}

console.log('//Soal 2 (switch-case)');

var hari = 21;
var bulan = 1;
var tahun = 1945;
var bulanStr = '';

switch (bulan) {
    case 1: bulanStr = 'Januari'; break;
    case 2: bulanStr = 'Februari'; break;
    case 3: bulanStr = 'Maret'; break;
    case 4: bulanStr = 'April'; break;
    case 5: bulanStr = 'Mei'; break;
    case 6: bulanStr = 'Juni'; break;
    case 7: bulanStr = 'Juli'; break;
    case 8: bulanStr = 'Agustur'; break;
    case 9: bulanStr = 'September'; break;
    case 10: bulanStr = 'Oktober'; break;
    case 11: bulanStr = 'November'; break;
    case 12: bulanStr = 'Desember'; break;
}

console.log(hari + ' ' + bulanStr + ' ' + tahun);